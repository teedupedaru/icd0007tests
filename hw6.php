<?php

require_once('common.php');

const BASE_URL = 'http://localhost:8080';

class Hw6Tests extends HwTests {

    function baseUrlResponds() {
        $this->get(BASE_URL);
        $this->assertResponse(200);
    }

    function listPageHasMenuWithCorrectLinks() {
        $this->get(BASE_URL);

        $this->assertLinkById('list-page-link');
        $this->assertLinkById('add-page-link');
    }

    function addPageHasCorrectElements() {
        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $this->assertField('firstName');
        $this->assertField('lastName');
        $this->assertField('phone1');
        $this->assertField('phone2');
        $this->assertField('phone3');

        $this->assertField('submitButton');
    }

    function displaysErrorWhenSubmittingInvalidData() {

        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $this->clickSubmitByName('submitButton');

        $this->assertPattern('/id\s*=\s*["\']error-block["\']/',
            "can't find element with id: 'error-block'");
    }

    function whenSubmittingInvalidDataDisplayedFormIsFilledWithPreviousData() {
        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $this->setFieldByName('firstName', 'a');
        $this->setFieldByName('lastName', 'b');
        $this->setFieldByName('phone1', 'c');
        $this->setFieldByName('phone2', 'd');
        $this->setFieldByName('phone3', 'e');

        $this->clickSubmitByName('submitButton');

        $this->assertFieldByName('firstName', 'a');
        $this->assertFieldByName('lastName', 'b');
        $this->assertFieldByName('phone1', 'c');
        $this->assertFieldByName('phone2', 'd');
        $this->assertFieldByName('phone3', 'e');
    }

    function clickingOnPersonNameTakesToFilledEditForm() {
        $person = $this->insertPerson();

        $this->clickLink($person->firstName);

        $this->assertFieldByName('firstName', $person->firstName);
        $this->assertFieldByName('lastName', $person->lastName);
        $this->assertFieldByName('phone1', $person->phone1);
        $this->assertFieldByName('phone2', $person->phone2);
        $this->assertFieldByName('phone3', $person->phone3);
    }

    function supportsUpdatingExistingPersonData() {
        $person = $this->insertPerson();

        $this->clickLink($person->firstName);

        $updatedPhone = strrev($person->phone1);

        $this->setFieldByName('phone1', $updatedPhone);

        $this->clickSubmitByName('submitButton');

        $this->assertText($updatedPhone);
        $this->assertNoText($person->phone1);
    }

    private function insertPerson() {
        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $person = getSampleData();

        $this->setFieldByName('firstName', $person->firstName);
        $this->setFieldByName('lastName', $person->lastName);
        $this->setFieldByName('phone1', $person->phone1);
        $this->setFieldByName('phone2', $person->phone2);
        $this->setFieldByName('phone3', $person->phone3);

        $this->clickSubmitByName('submitButton');

        $this->assertText($person->firstName);
        $this->assertText($person->lastName);
        $this->assertText($person->phone1);
        $this->assertText($person->phone2);
        $this->assertText($person->phone3);

        return $person;
    }
}

(new Hw6Tests())->run(new PointsReporter());
